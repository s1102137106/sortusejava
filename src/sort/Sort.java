/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort;

/**
 *
 * @author joenice
 */
public abstract class Sort {

    protected int[] data;

    public Sort() {
        data = new int[30];
    }

    public void Show() {
        System.out.println(this);
        for (int i = 0; i < this.data.length; i++) {
            System.out.printf("%d ", this.data[i]);
        }  
        System.out.println("");
    }

    public void Swap(int x, int y) {
        int temp = this.data[x];
        this.data[x] = this.data[y];
        this.data[y] = temp;
    }

    public void SetData(int[] data) {
        int[] temp = new int[30];
        temp = data.clone();
        this.data = temp;
    }

    abstract public void Sorting();
}
