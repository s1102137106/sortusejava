/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author joenice
 */
public class SortProgram {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {

        Sort[] sortArr = new Sort[10];
        //FileReader f = new FileReader("dd");
        int[] testData = {5, 3, 1, 6, 7, 2, 4};

        sortArr[0] = new BubbleSort();
        sortArr[1] = new InsertionSort();
        sortArr[2] = new SelectionSort();
        sortArr[3] = new QuickSort();
        sortArr[3] = new HeapSort();
        sortArr[3] = new MargeSort();
        
        for (Sort sortItem : sortArr) {
            if (sortItem == null) {
                break;
            }
            sortItem.SetData(testData);
            sortItem.Sorting();
            sortItem.Show();
        }

    }

}
