/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort;

/**
 *
 * @author joenice
 */
public class BubbleSort extends Sort {

    @Override
    public void Sorting() {
        for (int i = 0; i < this.data.length; i++) {
            for (int j = 0; j < this.data.length - i - 1; j++) {
                if (this.data[j] > this.data[j + 1]) {
                    this.Swap(j, j + 1);
                }
            }
        }
    }
}
