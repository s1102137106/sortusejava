/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort;

/**
 *
 * @author joenice
 */
public class HeapSort extends Sort {

    @Override
    public void Sorting() {
        //build MaxHeapTree
        for (int i = this.data.length / 2; i >= 0; i--) {
            MaxHeapify(i, this.data.length);
        }

        // sorting
        for (int i = this.data.length - 1; i > 0; i--) {
            this.Swap(0, i);
            MaxHeapify(0, i);
        }
    }

    private void MaxHeapify(int index, int range) {
        int left = (index + 1) * 2;
        int right = (index + 1) * 2 - 1;
        int maxIndex = index;
        
        if (left < range && this.data[left] > this.data[maxIndex]) {
            maxIndex = left;
        }

        if (right < range && this.data[right] > this.data[maxIndex]) {
            maxIndex = right;
        }

        if (maxIndex != index) {
            this.Swap(maxIndex, index);
            MaxHeapify(maxIndex,range);
        }

    }

}
