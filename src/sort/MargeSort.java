/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort;

/**
 *
 * @author joenice
 */
public class MargeSort extends Sort {

    @Override
    public void Sorting() {
        RecursiveSplitData(0, data.length - 1);
    }

    public void RecursiveSplitData(int left, int right) {
        if (left < right) {
            int mid = (left + right) / 2;
            RecursiveSplitData(left, mid);
            RecursiveSplitData(mid + 1, right);
            DoMarge(left, mid+1, right);
        }
    }

    public void DoMarge(int left, int mid, int right) {
        int[] temp = new int[data.length];
        int tempIndex = left;
        int leftEnd = mid -1;
        int num_elements = (right - left + 1);
        
        while (left <= leftEnd && mid <= right) {
            if (data[left] < data[mid]) {
                temp[tempIndex++] = data[left++];
            } else {
                temp[tempIndex++] = data[mid++];
            }
        }
        
        //補齊右邊
        while(left <= leftEnd){
            temp[tempIndex++] = data[left++];
        }
        
        //補齊左邊
        while(mid <= right){
             temp[tempIndex++] = data[mid++];
        }
        
        //轉換成新陣列
        while(num_elements>0){
            data[right] = temp[right--];
            num_elements--;
        }
    }

}
