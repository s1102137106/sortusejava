/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort;

/**
 *
 * @author joenice
 */
public class QuickSort extends Sort {

    @Override
    public void Sorting() {
        QuickSort(0, this.data.length - 1);
    }

    @SuppressWarnings("empty-statement")
    private void QuickSort(int left, int right) {
        if (left < right) {
            int mid = this.data[(left + right) / 2];//中間切一半
            int i = left - 1;  //修正while 先+1的問題
            int j = right + 1; //修正while 先+1的問題
            while (true) {
                while (this.data[++i] < mid) ;  // 向右找 找到大於中間值的跳出迴圈
                while (this.data[--j] > mid) ;  // 向左找 找到小於間值的跳出迴圈

                if (i >= j) //已經全部交換正確位置
                {
                    break;
                }
                this.Swap(i, j);
            }
            QuickSort(left, i - 1);   // 對左邊進行遞迴
            QuickSort(j + 1, right);  // 對右邊進行遞迴
        }
    }


}
