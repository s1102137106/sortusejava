/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort;

/**
 *
 * @author joenice
 */
public class InsertionSort extends Sort {

    @Override
    public void Sorting() {
        int i, j, temp;
        for (i = 1; i < this.data.length; i++) {
            temp = this.data[i];//正在處理的數字
            for (j = i; j > 0 && temp < this.data[j - 1]; j--) {
                this.data[j] = this.data[j - 1];
            }
            this.data[j] = temp;
        }
    }
}
