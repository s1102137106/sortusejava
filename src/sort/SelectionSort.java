/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort;

/**
 *
 * @author joenice
 */
public class SelectionSort extends Sort {

    @Override
    public void Sorting() {
        for (int i = 0; i < this.data.length; i++) {
            int minIndex = i;//最小的數字
            for (int j = i + 1; j < this.data.length; j++) {
                if (this.data[j] < this.data[minIndex]) {
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                this.Swap(minIndex, i);
            }
        }

    }

}
